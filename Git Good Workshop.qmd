## *Vždyť já nic dalšího nepotřebuji...*

```{=html}
<style>
.flushright {
   text-align: right;
}
</style>
```

Představte si, že všichni společně píšeme nějaký článek a zkuste do ***Autorů*** dopsat své jméno:

<div style="margin: auto; width: 40%; margin-top: 5%"><a href="https://demo.hedgedoc.org/66aEjXhAR3aZovdt2L1bBQ?both">
{{< qrcode https://demo.hedgedoc.org/66aEjXhAR3aZovdt2L1bBQ?both width=400 height=400 >}}
</a></div>

::: {style="margin: auto; width:80%; text-align: center"}
[git-good-workshop.zapadlo.name/edit](https://demo.hedgedoc.org/66aEjXhAR3aZovdt2L1bBQ?both)
:::

## Nebo psaní závěrečných prací

![‘Piled Higher and Deeper’ by Jorge Cham](assets/notFinal.gif){height="550"}

# Řešení?

## Git


> Git je v informatice distribuovaný systém správy verzí (VCS) vytvořený Linusem Torvaldsem pro vývoj jádra Linuxu.
>
> ::: {.flushright style="font-size: smaller"}
> Wikipedia -- upraveno
> :::

. . . 

::: {.callout-note}
Ke stejným účelům slouží i Mercurial, Subversion (SVN) a GNU Bazaar -- ty jsou avšak **značně** méně používané.
:::

## Git | Architektura

![Distribuovanost gitu (schematicky)](drawio/global.drawio.svg)

## Git | Architektura

![](drawio/snapshots.drawio.svg){fig-align="center"}

. . .

Git si neukládá pouze změny v souborech, ale vždy aktuální verzi. Pokud se soubor nezmění, git si uloží pouze ukazatel na stejný soubor do poslední verze, ve které se změnil.

. . .

::: {.callout-note}
## Repozitář

Git si databázi (nejen) změněných souborů uchovává ve složce `.git/`.
:::

## Git | Názvosloví

Každá *verze* se v gitu nazývá **commit** a posloupnost po sobě jdoucích commitů se nazývá **branch** (větev). Celý projekt i s databází verzí (složka `.git/`) nazýváme **repozitář** (*repo*).

. . .

![Znázornění větvení v daném repozitáři](drawio/commits-branches.drawio.svg)

. . .

Sloučení 2 větví se nazývá **merge** a provádíme ho často pomocí tzv. **merge requestu**.

## Git | Postup

:::{.incremental}
1. uděláme nějaké změny -- např. přidáme `working_script.R` a `broken_script.R`
2. provedeme **stage** některých změn -- např. *"připravíme"* `working_script.R`
3. připravené změny zapíšeme (provedeme **commit**) s vhodným komentářem, např. *"Added the working_script.R"*
:::

. . .

::: {.callout-tip}
## Ignorované soubory

Některé soubory můžeme chtít ignorovat (např. velké datasety nebo citlivé údaje) -- to provedeme přidáním do `.gitignore` souboru
:::

## Git | Postup

`git init`
: inicializuje prázdný git repozitář v aktuální složce

`git add <file>`
: začne sledovat daný soubor (`*` pro libovolný soubor)

`git stage <file>`
: označí změny daného souboru jako *připravené*

`git commit`
: vytvoří nový commit z připravených změn

`git status`
: vypíše *připravené* a *nepřipravené* změny aktuálního repozitáře

## Git | Postup

![](drawio/git-workflow.drawio.svg){width=100% fig-align="center"}

::: {style="font-size: xx-large"}
Nesledované soubory / nezměněné soubory / změněné soubory / připravené změny / repozitář
:::

<!-- TODO: Move elsewhere -->
<!-- 
::: {.callout-tip}
## Odložení

Rozpracované změny si můžeme *odložit* (**stash**) mimo pracovní prostředí a pak se k nim vrátit (např. pomocí `git stash pop`).
::: -->

## Git ≠ GitHub

Git
: Jak jsme si již vysvětlili, git je nástroj na správu verzí.

GitHub
: GitHub je webová služba, která nabízí hostování vzdálených git repozitářů. Je to v principu úplně stejné jako mít vlastní server a na něm obyčejnou složku s git repozitářem, se kterým budeme synchronizovat lokální repozitář.

## Git | Vzdálené repozitáře

`git clone`
: naklonuje vzdálený repozitář do aktuální složky

`git fetch`
: stáhne nové informace ze vzdáleného repa

`git merge`
: sloučí 2 větve do sebe (např. `new-feature` a `main`)

`git pull`
: `git fetch` následovaný `git merge`

`git push`
: nahraje stávající verzi aktuální větve na vzdálené repo

## Git | Vzdálené repozitáře

![Ukázka postupu použití vzdáleného repozitáře](drawio/remote-workflow.drawio.svg){width=85%}

## Nástroje pro git | Webové služby

:::{.incremental}
- [GitHub](https://github.com) -- nejpopulárnější, není open-source, možnosti CI/CD a statických stránek
- [GitLab](https://gitlab.com) -- populární, open-source, možnosti CI/CD i vlastního hostování (nejčastější git server firem) a statických stránek
- [BitBucket](https://bitbucket.com) -- zaměřeno na firmy, integrace s Jirou, možnosti CI/CD
- [Sourceforge](https://sourceforge.net) -- starší, stále jej používají některé open-source projekty, nevyžaduje git
- [Codeberg](https://codeberg.org) ([Forgejo](https://forgejo.org/)) -- open-source, EU příspěvková org., možnosti CI/CD
:::

## Nástroje pro git
### Programy

:::{.nonincremental}
- [GitKraken](https://www.gitkraken.com/) -- placený, ale velmi silný nástroj
- [GitFiend](https://gitfiend.com/) -- zdarma, ne však open-source
- [GitHub Desktop](https://desktop.github.com/) -- dobře provázáno s GitHubem
- [Gitg](https://wiki.gnome.org/Apps/Gitg/) -- open-source git GUI pro Linux
:::

### Rozšíření

Do většiny populárních textových editorů a IDE, včetně RStudia, VS Code (viz také [plugin Gitlens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)), Matlabu...

# Git v praxi

## Instalace gitu

:::: {.columns}

::: {.column width="50%"}
**Windows**

Z oficiální stránky gitu lze stáhnout instalační soubor:

[https://git-scm.com/download/win](https://git-scm.com/download/win)

:::

::: {.column width="50%"}
**Linux**

Závislé na distribuci: 

:::{.nonincremental}
- distribuce založené na Ubuntu:
	`sudo apt-get install git` (nebo `git-all`)
- distribuce založené na Fedora/RHEL:
	`sudo yum install git`
:::
:::

::::

Na školních počítačích ve Windows je git nainstalován.


## Vytvoření repozitáře

:::: {.columns}

::: {.column width="50%"}
GitHub

![Tvorba projektu](images/pastebin/2024-02-13-23-47-22.png)
:::

::: {.column width="50%"}
GitLab

<!-- !["New project"](images/pastebin/2024-02-13-23-39-06.png) -->
![Volba šablony](images/pastebin/2024-02-13-23-40-23.png){.lightbox}
![Tvorba samotného projektu](images/pastebin/2024-02-13-23-43-19.png){.lightbox}
:::

::::

## Práce s gitem | Klonování repozitáře

. . .

![Získání URL adresy](images/pastebin/2024-02-14-16-51-24.png){fig-align="center"}

. . .

Naklonování na naše zařízení vytvořený repozitář
```bash
git clone https://gitlab.com/sceptri/git-workshop
```

## Práce s gitem | Klonování repozitáře

Nebo jej můžeme otevřít v textovém editoru -- ukážeme si postup v RStudiu

. . . 

Pod `File > New Project` nalezneme

:::: {.columns}

::: {.column width="50%"}
![](images/pastebin/2024-02-14-16-59-27.png)

:::

::: {.column width="50%"}
![](images/pastebin/2024-02-14-16-59-34.png)
:::

::::

## Práce s gitem | Příklad změn

V momentální chvíli bude potřeba přidat nebo editovat nějaký soubor. Na ukázku můžeme např. přidat `working_script.R`
```R
library(devtools) # CatterPlots installation
install_github("Gibbsdavidl/CatterPlots")

# For more fun, see https://github.com/Gibbsdavidl/CatterPlots
library(CatterPlots)
x <- -10:10
y <- -x^2 + 10
purr <- catplot(xs=x, ys=y, cat=3, catcolor='#000000FF')
cats(purr, -x, -y, cat=4, catcolor='#FF0000')
```
---

```{R}
#| echo: true
#| code-fold: false
#| code-overflow: wrap
#| code-copy: true
#| code-line-numbers: false
#| fig-cap: "Na stažení: [git-good-workshop.zapadlo.name/working_script.R](https://git-good-workshop.zapadlo.name/working_script.R)"
#| fig-height: 4.5
#| fig-width: 4.5
# For more fun, see https://github.com/Gibbsdavidl/CatterPlots
library(CatterPlots)
x <- -10:10
y <- -x^2 + 10
purr <- catplot(xs=x, ys=y, cat=3, catcolor='#000000FF')
cats(purr, -x, -y, cat=4, catcolor='#FF0000')
```

## Práce s gitem | Co dál?

1. v RStudiu najdeme záložku **Git** (viz `View > Show Git`)
2. přípravíme, tj. označíme jako *staged*, všechny změny, které chceme commitovat
	- status souborů se změnil z *untracked* na *added*
	- *terminál:* `git add working_script.R`
3. commitneme připravené změny s vhodnou zprávou, např. *"Added script which uses CatterPlots R package"*
	- *terminál:* `git commit -m "Added script which uses CatterPlots R package"`

## Práce s gitem | Commit dialog v RStudiu

![](images/pastebin/2024-02-14-18-27-51.png){.lightbox}

## Práce s gitem | Historie

![](images/pastebin/2024-02-14-22-04-37.png){fig-align="center" height="25%"}

. . .

Lokální `HEAD` ukazuje na náš nový commit, zatímco `origin/HEAD` (tj. `HEAD` na serveru) ukazuje na *Initial commit*, který udělal GitLab za nás.

## Práce s gitem | Push

V RStudiu *"pushneme"* lokální commity pomocí

![](images/pastebin/2024-02-14-22-07-57.png){fig-align="center"}

. . .

V příkazové řádce bychom použili příkaz `git push`.

. . .

Nově pak bude historie vypadat:

![](images/pastebin/2024-02-14-22-11-03.png){fig-align="center"}

. . .

To stejné bude i vidět i na GitLabu.

## Práce s gitem | Vytvoření nové větve

Na GitLabu pod `Code > Branches > New Branch` můžeme vytvořit novou větev

. . .

![](images/pastebin/2024-02-14-22-16-29.png){fig-align="center" height=200}


## Práce s gitem | Přepnutí na novou větev

V RStudiu můžeme jednoduše změnit větev pomocí

![](images/pastebin/2024-02-14-22-34-54.png){fig-align="center"}

## Samostatná práce

:::{.nonincremental}
1. ve větvi `change-color` změňte barvu koček konvexní paraboly
2. tyto změny "pushněte" na server na tuto větev
3. vytvořte novou větev, např. `change-shape-and-color` z `main`
4. změňte tvar koček a barvu (jinou než předtím) konvexní paraboly (zkuste hodnoty `1`, `2` nebo `3`)
5. tyto změny "pushněte" na server na tuto větev
:::

## Práce s gitem | Merge request

**Merge request** je "žádost" o sloučení dvou větví. Na GitHubu je to **Pull request**.

![](images/pastebin/2024-02-14-22-47-35.png){fig-align="center" .lightbox}

## Práce s gitem | Uzavření merge requestu

![](images/pastebin/2024-02-14-22-54-59.png){fig-align="center" .lightbox}

## Práce s gitem | Konflikt

**Samostatná práce** \
Vytvořte merge request na sloučení `change-shape-and-color` do `main`

. . .

Povšimněme si vzniklého konfliktu

![](images/pastebin/2024-02-14-22-59-23.png){fig-align="center" .lightbox}

## Práce s gitem | Řešení konfliktů

![](images/pastebin/2024-02-14-23-01-18.png){fig-align="center" .lightbox}

## Práce s gitem | Forks

**Situace** \
Našli jsme na GitLabu (nebo GitHubu apod.) nějaký užitečný repozitář, který bychom chtěli pozměnit pro naše potřeby

. . .

**Řešení: Fork!**\
Pomocí tlačítka Fork můžeme vytvořit kopii tohoto repozitáře v našem účtu

![](images/pastebin/2024-02-15-17-32-49.png){fig-align="center" .lightbox}


<!-- 
::: {.callout-tip}
## Oprávnění

Takto nejčastěji probíhá kooperace u open-source projektů:

1. vývojář si "forkne" daný projekt
2. udělá změny, commitne je a pushne do svého forku
3. vytvoří merge request ze svého forku do původního repozitáře projektu
::: -->

# Best practices

## *"It's NOT enough to be COMMITED"*

Při vývoji projektu je dobré používat **větve**. Zvláště pokud na projektu dělá více lidí.

. . .

Je tak možno mít rozpracovaných několik věcí zároveň a přitom i funkční verzi projektu na hlavní větvi. 

. . .

Dobré je i používat větve pro různá prostředí:

- `main` --- "produkce" (něco, co se nesmí rozbít)
- `testing` --- prostředí, do kterého se "mergují" hotové větve předtím, než se dostanou do produkce
- apod. --- např. `staging` pro krok mezi testovacím prostředí a produkcí

## Komentáře git commitů

![XKCD: Git Commit](images/pastebin/2024-02-15-17-40-14.png){fig-align="center" height="300"}

. . .

Je dobré psát stručné a srozumitelné komentáře ke commitům, aby bylo jasné, co se v nich změnilo a nikdo se nemusel dívat přímo do kodu

## Součásti každého repozitáře

- `README.md` -- krátký úvod do projektu, který by měl obsahovat:
  - **vysvětlení, co je to za projekt**
  - instrukce na spuštění
- `LICENSE` -- licence, pod kterou je projekt publikován
  - open-source licence jsou MIT, GPL, Apache a další
- `CHANGELOG.md`, `CONTRIBUTING.md`,...
  - (nejsou vždy potřeba nebo jsou občas součástí `README.md`)

## Tags & Releases

**Situace** \
Bylo by užitečné označit některý commit za význačný -- např. se může jednat o stabilní verzi projektu.

. . .

**Řešení: Tags** \
Štítky (*tags*) slouží právě k tomuto účelu -- umožňují ostatním se jednoduše zorientovat, jak by měli projekt naklonovat, aby měli stabilní verzi apod.

. . .

**Navíc: Releases** \
Zvláště u repozitáře nějakého programu můžeme chtít spolu s označením commitu stabilní verze ještě přiložit např. zkompilovaný kód -- přesně to "release" umožňuje.

# Závěrem

---

![XKCD: Git](https://imgs.xkcd.com/comics/git.png){.r-stretch}

## Zdroje

:::{.nonincremental}
- https://neurathsboat.blog/post/git-intro/
- https://gitbookdown.dallasdatascience.com/
- https://swcarpentry.github.io/git-novice/index.html
- https://git-scm.com/book/cs/v2/
:::